#!/usr/bin/env bash

# install venv
sudo apt-get install python3-venv

# create virtual env, activate venv, restore requirements
python3 -m venv venv && source ./venv/bin/activate && pip install -r requirements.txt
