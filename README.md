# openai_gym_projects

Various projects making use of the
 [openai gym](https://gym.openai.com/) toolkit for machine learning.

##   Setup

Once cloned, execute `init.sh`.  This will create a virtual environment
 used to execute the scripts in this project.  The virtual environment
 can be found in the `venv` directory, and can be enabled using
```bash
$ source ./venv/bin/activate
```